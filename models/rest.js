var mongoose = require('mongoose');
var restful = require('node-restful');
var app = require('../app')

var restfuls = app.resource = restful.model('products', mongoose.Schema({
    prod_name: String,
    prod_desc: String,
    prod_price: Number,
    updated_at: Date
  }))
  .methods(['get', 'post', 'put', 'delete']);

  module.exports = restfuls;